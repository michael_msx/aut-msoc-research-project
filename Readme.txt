This project is designed for simulating social network context by WS-Dream dataset

Before using this tool, please check followings:

Install

1. Python SDK   (3.5+)
2. Neo4j   (3.0.6+ ) (Java SDK 1.8 required)


We recommend using PyCharm as the IDE tool, otherwise you should manually config the dependency libraries.


In PyCharm , you could easily find and install third-party libraries in project setting window.


Dependency libraries:

# use latest version

1. numpy
2. py2neo
3. ConfigObj


A brief introduction

all the scripts all in script folder

step 0 cfg\conf.ini  configure your Neo4j DB stuff.

step 1 script\step1  Import data into Neo4j DB

step 2 script\step2  Generate relationships between user to user, ws to ws, and user to ws

step 3 script\step3  Calculate the matrix

#Note some matrix we want to try three attempts.
 Change to your real relationship name in the script before running it.



!! Delete Neo4j Data

Execute following cql query in Neo4j Web Console

Delete Nodes:

    Match (a:Node_name) Delete a

Delete Relationships:

    Match ()-[r:Relationship_name]-() delete r


Note: If you want to delete one node, you should delete all relationships on this node first.
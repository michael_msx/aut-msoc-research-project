import numpy as np
import sqlite3

dataSet = np.loadtxt('data/rtmatrix.txt')

conn = sqlite3.connect('ws.db')
cursor = conn.cursor()

cursor.execute('create table userWSRTMatrix (user_id INT, ws_id INT, rt_time real)')


for (x,y) ,val in np.ndenumerate(dataSet):

    sql = 'insert into userWSRTMatrix (user_id, ws_id, rt_time ) VALUES(?,?,?)'

    row = [x,y,val]
    cursor.execute(sql,row)


conn.commit()

conn.close()
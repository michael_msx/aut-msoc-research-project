from py2neo import Graph, Node

print("Use this script to create a new user node!")

user_Id = input("User Id: ")
ipAddr = input("User IP Address:")
country = input("Country: ")
longitude = input("Longitude: ")
latitude = input("Latitude: ")

graph = Graph(host="ec2-52-62-84-106.ap-southeast-2.compute.amazonaws.com", user="neo4j", password="2wsx1qaz")

tx = graph.begin()

user = Node("User", user_id=user_Id, ipAddress=ipAddr, country=country, longitude=longitude, latitude=latitude)
tx.create(user)

tx.commit()


print("Done!")
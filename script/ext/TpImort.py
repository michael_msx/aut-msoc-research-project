import numpy as np
import sqlite3

dataSet = np.loadtxt('data/tpmatrix.txt')

conn = sqlite3.connect('ws.db')
cursor = conn.cursor()

cursor.execute('create table userWSTPMatrix (user_id INT, ws_id INT, tp_time real)')


for (x,y) ,val in np.ndenumerate(dataSet):

    sql = 'insert into userWSTPMatrix (user_id, ws_id, tp_time ) VALUES(?,?,?)'

    row = [x,y,val]
    cursor.execute(sql,row)

conn.commit()

conn.close()
from py2neo import Graph
from configobj import ConfigObj

config = ConfigObj('../cfg/conf.ini')

db_address = config['DB_Config']['db_address']
db_user = config['DB_Config']['db_user']
db_pw = config['DB_Config']['db_pw']

graph = Graph(host=db_address,user=db_user,password=db_pw)


def identify_user_popularity(user_id, relation_name):

    cql = "match (u:User)-[:"+relation_name+"]->(o:User) where u.user_id='"+ str(user_id) +"' return count(o) as n"
    result1 = graph.data(cql)
    close_users = result1[0]['n']

    cql2 = "match (u:User) return count(u) as n"
    sum_users = graph.data(cql2)[0]['n']
    user_popularity = close_users / sum_users

    return user_popularity

def identify_ws_popularity(ws_id):
    cql = "match (u:WS)-[:SameProvider]->(o:WS) where u.ws_id='"+ str(ws_id) +"' return count(o) as n"
    result1 = graph.data(cql)
    close_ws = result1[0]['n']

    cql2 = "match (u:WS) return count(u) as n"
    sum_ws = graph.data(cql2)[0]['n']
    ws_popularity = close_ws / sum_ws

    return ws_popularity


def identify_user_activity(user_id, relation_name):
    cql = "match (u:User)-[:"+relation_name+"]->(o:WS) where u.user_id='" + str(user_id) + "' return count(o) as n"
    result1 = graph.data(cql)
    ws_has_used = result1[0]['n']

    cql2 = "match (u:WS) return count(u) as n"
    sum_ws = graph.data(cql2)[0]['n']
    user_activity = ws_has_used / sum_ws

    return user_activity


def identify_ws_market_share(ws_id, relation_name):
    cql = "match (u:User)-[:"+relation_name+"]->(o:WS) where o.ws_id='" + str(ws_id) + "' return count(u) as n"
    result1 = graph.data(cql)
    ws_has_used = result1[0]['n']

    cql2 = "match (u:User) return count(u) as n"
    sum_users = graph.data(cql2)[0]['n']
    ws_market_share = ws_has_used / sum_users

    return ws_market_share

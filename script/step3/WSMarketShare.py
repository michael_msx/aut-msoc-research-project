import numpy as np
from configobj import ConfigObj

from script.ext.Util import *

print("")

config = ConfigObj('../cfg/conf.ini')

user_number = int(config['user_ws_relation']['user_num'])
ws_number = int(config['user_ws_relation']['ws_num'])

user_matrix = []
ws_matrix= []

print("Web Service Market Share Matrix Export Process Start...")

for user_id in range(0,user_number):

    ws_marketShare_500 =  identify_ws_market_share(user_id, 'Used500')
    ws_marketShare_1000 = identify_ws_market_share(user_id, 'Used500')   # change to real relation name
    ws_marketShare_1500 = identify_ws_market_share(user_id, 'Used500')   # change to real relation name

    user_temp_list = [user_id, ws_marketShare_500, ws_marketShare_1000 , ws_marketShare_1500]

    user_matrix.append(user_temp_list)

user_array = np.array(user_matrix, dtype=float)


np.savetxt("../../result/wsMarketShare",user_array, fmt='%d %1.4f %1.4f %1.4f')

print("Web Service Market Share Matrix Export Process Finish")

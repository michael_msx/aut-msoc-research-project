import numpy as np
from configobj import ConfigObj

from script.ext.Util import *

print("")

config = ConfigObj('../cfg/conf.ini')

user_number = int(config['user_ws_relation']['user_num'])
ws_number = int(config['user_ws_relation']['ws_num'])

user_matrix = []
ws_matrix= []

print("User Activity Matrix Export Process Start...")

for user_id in range(0,user_number):

    user_activity500  = identify_user_activity(user_id, 'Used500')
    user_activity1000 = identify_user_activity(user_id, 'Used500')   # Change to Real relation name
    user_activity1500 = identify_user_activity(user_id, 'Used500')   # Change to Real relation name

    user_temp_list = [user_id, user_activity500, user_activity1000 , user_activity1500]

    user_matrix.append(user_temp_list)

user_array = np.array(user_matrix, dtype=float)


np.savetxt("../../result/userActivity",user_array, fmt='%d %1.8f %1.8f %1.8f')

print("User Activity Matrix Export Process Finish")

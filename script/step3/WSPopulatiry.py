import numpy as np
from configobj import ConfigObj

from script.ext.Util import *

print("")

config = ConfigObj('../cfg/conf.ini')

user_number = int(config['user_ws_relation']['user_num'])
ws_number = int(config['user_ws_relation']['ws_num'])

user_matrix = []
ws_matrix= []

print("Web Service Matrix Export Process Start")

for ws_id in range(0,ws_number):

    ws_popularity = identify_ws_popularity(ws_id)

    ws_temp_list = [ws_id,ws_popularity]
    ws_matrix.append(ws_temp_list)

ws_array = np.array(ws_matrix, dtype=float)

np.savetxt("../../result/wsPopularity",ws_array, fmt='%d %1.8f')

print("Web Service Matrix Export Process Finish")
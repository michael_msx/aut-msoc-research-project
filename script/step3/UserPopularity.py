import numpy as np
from configobj import ConfigObj

from script.ext.Util import *

print("")

config = ConfigObj('../cfg/conf.ini')

user_number = int(config['user_ws_relation']['user_num'])
ws_number = int(config['user_ws_relation']['ws_num'])

user_matrix = []
ws_matrix= []

print("User Matrix Export Process Start...")

for user_id in range(0,user_number):

    user_popularity25  = identify_user_popularity(user_id,'CloseTo')
    user_popularity50  = identify_user_popularity(user_id,'CloseTo')     # Change to the real relation name
    user_popularity100 = identify_user_popularity(user_id,'CloseTo')     # Change to the real relation name

    user_temp_list = [user_id, user_popularity25,user_popularity50 , user_popularity100]

    user_matrix.append(user_temp_list)

user_array = np.array(user_matrix, dtype=float)


np.savetxt("../../result/userPopularity",user_array, fmt='%d %1.3f %1.3f %1.3f')

print("User Matrix Export Process Finish")

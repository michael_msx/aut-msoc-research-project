from py2neo import Graph
import configparser

config = configparser.ConfigParser()
config.read("../cfg/conf.ini",encoding='utf-8')

longitude_limit = config['user_relationship']['longitude']
latitude_limit = config['user_relationship']['latitude']
relation_name = config['user_relationship']['relation_name']

db_address = config['DB_Config']['db_address']
db_user = config['DB_Config']['db_user']
db_pw = config['DB_Config']['db_pw']


graph = Graph(host=db_address,user=db_user,password=db_pw)

# Get current user country list
print("Retrieve User Country list...")

user_country_list = graph.data("Match (a:User) return distinct a.country as country")

print("User Relationship Init Process Start")

tx = graph.begin()

for obj in user_country_list:

    cql = ("Match (a:User {country:'"+obj['country']+"'}), (b:User {country:'"+obj['country']+"'}) "
           "where a.user_id <> b.user_id and abs(toFloat(a.longitude) - toFloat(b.longitude))<=" + longitude_limit +
            " and abs(toFloat(a.latitude) - toFloat(b.latitude))<=" + latitude_limit+" create (a)-[r:"+relation_name+"]->(b)")
    tx.append(cql)
tx.commit()

print("End")
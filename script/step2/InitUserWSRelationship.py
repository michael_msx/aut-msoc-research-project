import sqlite3
from bean.relation import Relation
from py2neo import Graph, Relationship
from configobj import ConfigObj

config = ConfigObj('../cfg/conf.ini')

user_number = int(config['user_ws_relation']['user_num'])
rp_limit = config['user_ws_relation']['rp_time_limit']
tp_limit = config['user_ws_relation']['tp_time_limit']
relation_name = config['user_ws_relation']['relation_name']

db_address = config['DB_Config']['db_address']
db_user = config['DB_Config']['db_user']
db_pw = config['DB_Config']['db_pw']

def get_relations():


    conn = sqlite3.connect('../ws.db')

    relation_list = []

    for user_id in range(0, user_number):

        print("init relationships for user id " + str(user_id))

        cursor = conn.cursor()

        param = [user_id, rp_limit]

        param_tp = [user_id , tp_limit]

        cursor.execute('select * from userWSRTMatrix  where rt_time > 0 AND user_id = ? ORDER BY  rt_time asc LIMIT ?',param)

        resultRows = cursor.fetchall()

        rs_rp = {}
        rs_tp = {}

        ws_rt_list = []
        ws_tp_list = []

        for ws_obj in enumerate(resultRows):
            if ws_obj[1][1] not in rs_rp:
                rs_rp.update({ws_obj[1][1]:ws_obj[1][2]})
                ws_rt_list.append(ws_obj[1][1])

        cursor2 = conn.cursor()

        cursor2.execute('select * from userWSTPMatrix  where tp_time > 0 AND user_id = ? ORDER BY  tp_time DESC LIMIT ?',param_tp)

        resultRows2 = cursor2.fetchall()

        for ws_obj2 in enumerate(resultRows2):
            if ws_obj2[1][1] not in rs_tp:
                rs_tp.update({ws_obj2[1][1]:ws_obj2[1][2]})
                ws_tp_list.append(ws_obj2[1][1])


        ws_join = set(ws_rt_list).intersection(ws_tp_list)

        for ws_id in ws_join:

            relation = Relation()

            relation.user_id = user_id
            relation.ws_id =  ws_id
            relation.rp_time = rs_rp[ws_id]
            relation.tp_time = rs_tp[ws_id]

            relation_list.append(relation)

    return relation_list


rs = get_relations()

print("\n\nUser WS Relationship Init")
print(str(len(rs)) + " relationships create" )

print("Graph Database work start")

graph = Graph(host=db_address,user=db_user,password=db_pw)

for obj in rs:
    user_node = graph.find_one("User", property_key='user_id', property_value=str(obj.user_id))
    ws_node = graph.find_one("WS", property_key='ws_id', property_value=str(obj.ws_id))

    if not ((user_node is None) or (ws_node is None) ):
        user_used_ws = Relationship(user_node,relation_name,ws_node, rp_time=str(obj.rp_time),tp_time=str(obj.tp_time))
        graph.create(user_used_ws)

print("End")
from py2neo import Graph, Node , Relationship
from configobj import ConfigObj

#same country and provider


config = ConfigObj('../cfg/conf.ini')

db_address = config['DB_Config']['db_address']
db_user = config['DB_Config']['db_user']
db_pw = config['DB_Config']['db_pw']


graph = Graph(host=db_address,user=db_user,password=db_pw)

# Get current user country list

print("Retrieve User Country list...")

print("Create WS Relationship by same country and service provider")

user_country_list = graph.data("Match (w:WS) return distinct w.country as country")

tx = graph.begin()

for obj in user_country_list:

    cql = "Match (a:WS {country:'"+obj['country']+"'}), (b:WS {country:'"+obj['country']+"'}) where a.ws_id <> b.ws_id and a.provider=b.provider create (a)-[r:SameProvider]->(b)"

    tx.append(cql)
tx.commit()

print("END")

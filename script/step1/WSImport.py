from bean.ws import WSBean

from py2neo import Graph, Node
from configobj import ConfigObj

path = "../data/wslist.txt"

wsList = []

print("Read Web Service List Start!");

with open(path) as f:
    for line in f:
        tempWS = WSBean(line)
        wsList.append(tempWS)

print("Read WS List Completed! %d web services have been imported!" % (len(wsList)));


print("Import WS List Start!");

config = ConfigObj('../cfg/conf.ini')

db_address = config['DB_Config']['db_address']
db_user = config['DB_Config']['db_user']
db_pw = config['DB_Config']['db_pw']

graph = Graph(host=db_address,user=db_user,password=db_pw)

tx = graph.begin()


for obj in wsList:
    ws = Node("WS", ws_id=obj.ws_Id, wsdl_address=obj.wsdl_addr, provider=obj.provider, country=obj.country)
    tx.create(ws)

tx.commit()

print("Import WS List Completed!");
from bean.user import Users
from py2neo import Graph, Node
from configobj import ConfigObj


path = "../data/userlist.txt"

userList = []

print("Read User List Start!");

with open(path) as f:
    for line in f:
        tempUser = Users(line)
        userList.append(tempUser)

print("Read User List Completed! %d users have been imported!" % (len(userList)));


print("Import User List Start!");

config = ConfigObj('../cfg/conf.ini')

db_address = config['DB_Config']['db_address']
db_user = config['DB_Config']['db_user']
db_pw = config['DB_Config']['db_pw']

graph = Graph(host=db_address,user=db_user,password=db_pw)

tx = graph.begin()

for obj in userList:
    user  = Node("User", user_id=obj.Id,ipAddress=obj.ipAddr,country=obj.country,longitude=obj.longitude,latitude=obj.latitude)
    tx.create(user)

tx.commit()

print("Import User List Completed!");
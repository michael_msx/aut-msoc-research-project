import re


class WSBean():

    def __init__(self):
        self.ws_Id = None
        self.wsdl_addr = None
        self.provider = None
        self.country = None

    def __init__(self, line):
        array = re.split('\t',line)

        self.ws_Id = array[0].strip()
        self.wsdl_addr = array[1].strip()
        self.provider = array[2].strip()
        self.country = array[3].strip()